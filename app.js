const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
//const Product = require('./models/Product');
const Offre = require ('./models/Offre');
const queryString = require ('querystring');

app.use(bodyParser.urlencoded({ extended: false }));

// views setting
app.set('view engine', 'ejs');
//public elements
app.use('/public', express.static('public'));

// connexion à mongoose
mongoose.set('useCreateIndex', true);
mongoose.connect('mongodb+srv://sprtck:Bolor@kfe69@cluster0-l9dxp.mongodb.net/test?retryWrites=true&w=majority',
  { useNewUrlParser: true,
    useUnifiedTopology: true })
  .then(() => console.log('Connexion à MongoDB réussie !'))
  .catch(() => console.log('Connexion à MongoDB échouée !'));


// CORS
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
});

app.use(bodyParser.json());



app.get('/offres/:url', (req, res, next) => {
  var annonce =  Offre.findOne({url : req.params.url}, function(err, annonce){
    if(!err){
      //console.log(annonce);
       res.render('offre', annonce);
    } else {
      console.log(err)
      error => res.status(404).json({ error });
    }
  }); 
});
 
app.get('/offres', (req, res, next) => {
  var page = req.query.page;
  let {secteur, experience, formation} = req.query;
  let query = {};

  if (secteur != null) query.secteur = secteur;
  if (experience != null) query.experience = {$gte : experience};
  if (formation != null) query.formation = formation;   
  const limit = 10;
  var annoncesToShow = [];
  
  if (page == undefined) {
    page = 1;
  }  
  const startIndex = (page-1) * limit;
  const endIndex = page * limit;

  //console.log("page" + page + "start : " + startIndex+ "end : "+ endIndex );

  var annonces =  Offre.find(query, function (error, annonces ) {
      if (!error) {
        // on initialise un nouveau tableau qu'on renvoie en fonction des filtres 
        annoncesToShow = annonces;

        var nbPages = Math.trunc(annoncesToShow.length/limit) + 1;
        annoncesToShow = annoncesToShow.slice(startIndex, endIndex);

        res.render('nos_offres', { annonces : annoncesToShow, current : page, pages : nbPages });
      }
      else {
        error => res.status(404).json({ error });
      }
  }).sort({ date : -1 });

  //.then(offres => res.render('nos_offres'), res.json(offres))
  //.catch( error => res.json({ error }));
})

app.get('/', (req, res, next) => {
  res.render('index');
});

app.get('/API/offres', (req, res, next) => {

  //console.log("requete get");
  
  Offre.find().sort({date : -1})
  .then(offres => res.status(201).json({offres : offres}))
  .catch(error => res.status(401).json({ error}));
});


app.post('/offres/admin', (req, res, next) => {
  const offre = new Offre({...req.body});
  console.log(offre);
  offre.url = setUrl(offre.reference, offre.titre)
  
  offre.save()
  .then( () => res.json(offre))
  .catch( error => res.json({ error }));
});

app.delete('/offres/admin/:ref', (req, res, next) =>{
  Offre.deleteOne({ reference : req.params.ref})
  .then(res.json({ message : "annonce supprimée" }))
  .catch(error => res.json({ error }));
});




// fonction dont on se sert dans post pour définir un URL unique pour l'offre. Lisible par l'utilisateur
// ( Ref-Titre-de-Lannonce)
function setUrl(ref, titre) {
  // on concat la ref et le titre avec un "-" et on remplace les " " par des "-" dans le titre pour faire une url unique
  return ref.concat("-",titre).replace(/ /g, "-");
}







/******** BDD Produits tuto 
// post d'un produit envoyé sous format JSON
app.post('/api/products/', (req, res, next) => {
    //delete req.body._id;
    const product = new Product({
      ...req.body
    });

    console.log('on tente de poster un produit' + product);

    product.save()
      .then(() => res.status(201).json({product}))
      .catch(error => res.status(400).json({ error }));
      });


// Put sous la forme d'un produit format JSON avec id en parametre route
app.put('/api/products/:id', (req, res, next) => {
    console.log("on tente de modifier :" + req.params.id);
    
    Product.updateOne({_id: req.params.id}, {...req.body, _id : req.params.id})
    .then(res.status(201).json({message: 'Modified!'}))
    .catch(res.status(400).json({ error }));
});


app.delete('/api/products/:id=/', (req, res, next) => {

  console.log("tentative de supprimer");
  
    Product.deleteOne({_id : req.params.id})
    .then(res.status(201).json({messsage : "message supprimé !"}))
    .catch(res.status(400).json({ error }));
})


/*
app.delete('/delete/:id', function(req, res) {
  var id = req.param("id");
      MyModel.remove({
          _id: id 
      }, function(err){
          if (err) {
              console.log(err)
          }
          else {
             return res.send("Removed");
          }
      });
  });


// get products/id
app.get('/api/products/:id', (req, res, next) => {
    console.log('on essaye d en trouver un : ' + req.params.id) + res;
    Product.findOne({ _id: req.params.id })
    .then(product => res.status(201).json({product : product}))
    .catch(error => res.status(404).json({ error }))
  });

// get dans le rout products
app.get('/api/products', (req, res, next) => {
    console.log("on tente de récupérer tous les produits");
    
    Product.find()
      .then(product => res.status(200).json({ products: product }))
      .catch(error => res.status(400).json({ error }));
  });   

  */


module.exports = app;